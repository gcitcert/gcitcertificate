import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Colors from "../Constants/Color";
import Login from "../Screens/Login";
import LoginNavigator from "./LoginNavigator";

const Stack = createNativeStackNavigator();


function MainNavigator() {
  return (
    <Stack.Navigator
      screenOptions={{
        contentStyle: { backgroundColor: Colors.background },
        headerShown: false,
        headerStyle: { backgroundColor: Colors.secondary },
      }}
    >
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="LoginNavigator" component={LoginNavigator}/>

    </Stack.Navigator>
  );
}
export default MainNavigator;
